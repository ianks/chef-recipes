def run(envs)
  envs = env.map { |k, v|
    "-e #{k}=#{v}"
  }.join(" ")

  log "Docker environment: #{envs}"

  script "run_app_elasticsearch_container" do
    interpreter "bash"
    user "root"

    code <<-EOH
      docker run -v /data:/data --name es-data busybox true
    EOH

    code <<-EOH
      docker run -d -p 9300:9300 -p 9200:9200 --volumes-from es-data --name elasticsearch -e AWS_ACCESS_KEY=${AWS_ACCESS_KEY} -e AWS_SECRET_KEY=${AWS_SECRET_KEY} adgenomicsdev/elasticsearch-ec2-cluster
    EOH
  end
end
